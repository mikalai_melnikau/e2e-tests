package com.autotests.test.sslv;

import com.autotests.product.sslv.ad.AdvertisementCategory;
import com.autotests.product.sslv.ad.bo.Advertisement;
import com.autotests.product.sslv.ad.service.AdvertisementUiService;
import com.autotests.product.sslv.common.Actor;
import com.autotests.product.sslv.common.Language;
import com.autotests.product.sslv.search.Field;
import com.autotests.product.sslv.search.Filter;
import com.autotests.product.sslv.search.Search;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.util.List;

public class SaveAdvertisementsInBookmarksTest {

    private AdvertisementUiService advertisementUiService = new AdvertisementUiService();

    private List<Advertisement> ads;

    @BeforeClass(description = "Given Actor as anonymous user with RU locale")
    public void actorAsAnonymousUserWithRuLocale() {
        Actor.asAnonymousUser(Language.RU);
        Actor.navigateToCategory(AdvertisementCategory.ELECTRONICS);
    }

    @BeforeClass(description = "When Actor discovers some advertisements",
            dependsOnMethods = "actorAsAnonymousUserWithRuLocale")
    public void whenActorDiscoversSomeAdvertisements() {
        Actor.navigateToSearch();
        Search.by(new Filter().by(Field.DESCRIPTION, "Computer").by(Field.LOCATION, "Рига"));
    }


    @BeforeClass(description = "And add some results to bookmarks",
            dependsOnMethods = "whenActorDiscoversSomeAdvertisements")
    @Parameters("adsCount")
    public void andAddSomeResultsToBookmarks(int adsCount) {
        ads = advertisementUiService.readRandomAds(adsCount);
        advertisementUiService.addToBookmarks(ads);
    }

    @Test(description = "Then advertisements are saved in bookmarks")
    public void thenAdvertisementsAreSavedInBookmarks() {
        Actor.navigateToBookmarks();
        List<Advertisement> adsInBookmarks = advertisementUiService.readAds();
        Assert.assertTrue(ads.containsAll(adsInBookmarks));
    }
}
