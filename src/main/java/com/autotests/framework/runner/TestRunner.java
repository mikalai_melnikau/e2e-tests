package com.autotests.framework.runner;

import com.autotests.framework.config.SeleniumConfig;
import com.autotests.framework.config.TestConfig;
import com.autotests.framework.logger.Logger;
import com.beust.jcommander.JCommander;
import com.epam.reportportal.testng.ReportPortalTestNGListener;
import org.testng.TestNG;

public class TestRunner {

    public static void main(String[] args) {
        Logger.trace("----------------------------------------");
        Logger.trace("             E2E AUTO-TESTS             ");
        Logger.trace("----------------------------------------");

        Object[] options = {
                new TestConfig(),
                new SeleniumConfig()
        };
        JCommander jCommander = new JCommander(options);
        jCommander.setProgramName("e2e-tests");
        jCommander.usage();
        jCommander.parse(args);

        Logger.trace("----------------------------------------");
        Logger.trace("                TESTING                 ");
        Logger.trace("----------------------------------------");
        runTestNg();
    }

    public static void runTestNg() {
        TestNG testNg = new TestNG();
        testNg.addListener(new TestListener());
        testNg.addListener((Object) new ReportPortalTestNGListener());
        testNg.setTestSuites(TestConfig.getSuites());
        testNg.run();
    }
}
