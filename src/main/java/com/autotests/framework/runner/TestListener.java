package com.autotests.framework.runner;

import com.autotests.framework.logger.Logger;
import com.autotests.framework.ui.Browser;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

public class TestListener extends TestListenerAdapter {

    private static String getTestMethodName(ITestResult result) {
        return result.getTestClass().getRealClass().getSimpleName() + "." + result.getMethod().getMethodName();
    }

    @Override
    public void onStart(ITestContext context) {
        Logger.info("[TEST STARTED] " + context.getName());
    }

    @Override
    public void onFinish(ITestContext context) {
        Logger.info("[TEST FINISHED] " + context.getName());
        Browser.getBrowser().stop();
    }

    @Override
    public void onConfigurationSkip(ITestResult result) {
        Logger.info("[CONFIGURATION SKIPPED] " + getTestMethodName(result));
    }

    @Override
    public void beforeConfiguration(ITestResult result) {
        Logger.info("[CONFIGURATION STARTED] " + getTestMethodName(result));
    }

    @Override
    public void onConfigurationFailure(ITestResult result) {
        Logger.info("[CONFIGURATION FAILED] " + getTestMethodName(result), result.getThrowable());
    }

    @Override
    public void onConfigurationSuccess(ITestResult result) {
        Logger.info("[CONFIGURATION SUCCESS] " + getTestMethodName(result));
    }

    @Override
    public void onTestSkipped(ITestResult result) {
        Logger.info("[METHOD SKIPPED] " + getTestMethodName(result));
    }

    @Override
    public void onTestStart(ITestResult result) {
        Logger.info("[METHOD STARTED] " + getTestMethodName(result));
    }

    @Override
    public void onTestFailure(ITestResult result) {
        Logger.info("[METHOD FAILED] " + getTestMethodName(result), result.getThrowable());
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        Logger.info("[METHOD SUCCESS] " + getTestMethodName(result));
    }
}
