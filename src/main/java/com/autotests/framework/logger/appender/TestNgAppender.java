package com.autotests.framework.logger.appender;

import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.spi.LoggingEvent;
import org.testng.Reporter;

public class TestNgAppender extends AppenderSkeleton {

    @Override
    protected void append(LoggingEvent event) {
        String message = this.layout.format(event);
        Reporter.log(message);
    }

    @Override
    public void close() {
    }

    @Override
    public boolean requiresLayout() {
        return true;
    }
}
