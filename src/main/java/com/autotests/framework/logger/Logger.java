package com.autotests.framework.logger;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.log4j.LogManager;

import static com.autotests.framework.logger.LoggerLevel.*;

public class Logger {

    public static org.apache.log4j.Logger getLogger() {
        return LogManager.getRootLogger();
    }

    public static void fatal(String message) {
        getLogger().fatal(escapeMessage(message));
    }

    public static void fatal(String message, Throwable cause) {
        getLogger().fatal(escapeMessage(message), cause);
    }

    public static void error(String message) {
        getLogger().error(escapeMessage(message));
    }

    public static void error(String message, Throwable cause) {
        getLogger().error(escapeMessage(message), cause);
    }

    public static void warn(String message) {
        getLogger().warn(escapeMessage(message));
    }

    public static void warn(String message, Throwable cause) {
        getLogger().warn(escapeMessage(message), cause);
    }

    public static void operation(String message) {
        getLogger().log(OPERATION, escapeMessage(message));
    }

    public static void action(String message) {
        getLogger().log(ACTION, escapeMessage(message));
    }

    public static void binary(Object object) {
        getLogger().log(BINARY, object);
    }

    public static void info(String message) {
        getLogger().info(escapeMessage(message));
    }

    public static void info(String message, Throwable cause) {
        getLogger().info(escapeMessage(message), cause);
    }

    public static void debug(String message) {
        getLogger().debug(escapeMessage(message));
    }

    public static void debug(String message, Throwable cause) {
        getLogger().debug(escapeMessage(message), cause);
    }

    public static void trace(String message) {
        getLogger().trace(escapeMessage(message));
    }

    public static void trace(String message, Throwable cause) {
        getLogger().trace(escapeMessage(message), cause);
    }

    public static void shutdown() {
        LogManager.shutdown();
    }

    private static String escapeMessage(String s) {
        return StringEscapeUtils.escapeHtml4(s);
    }
}
