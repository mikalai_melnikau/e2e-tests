package com.autotests.framework.logger;

import org.apache.log4j.Level;

public class LoggerLevel extends Level {

    public static final int BINARY_INT = INFO_INT + 1000;
    public static final int ACTION_INT = INFO_INT + 2000;
    public static final int OPERATION_INT = INFO_INT + 3000;

    public static final Level BINARY = new LoggerLevel(BINARY_INT, "HTML_OUTPUT", 6);
    public static final Level ACTION = new LoggerLevel(ACTION_INT, "ACTION", 6);
    public static final Level OPERATION = new LoggerLevel(OPERATION_INT, "OPERATION", 6);

    protected LoggerLevel(int level, String levelStr, int syslogEquivalent) {
        super(level, levelStr, syslogEquivalent);
    }
}
