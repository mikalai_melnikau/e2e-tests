package com.autotests.framework.config;

import com.autotests.framework.ui.BrowserType;
import com.beust.jcommander.IStringConverter;
import com.beust.jcommander.Parameter;
import lombok.Getter;

import java.net.MalformedURLException;
import java.net.URL;

public class SeleniumConfig {

    private static final String HUB_URL_PATTERN = "http://%s:%s/wd/hub";

    @Parameter(names = {"--selenium_host", "-sh"}, description = "Selenium server host")
    @Getter
    private static String host = "127.0.0.1";

    @Parameter(names = {"--selenium_port", "-sp"}, description = "Selenium server port")
    @Getter
    private static String port = "4444";

    @Parameter(names = {"--browser", "-b"}, description = "Browser type")
    @Getter
    private static BrowserType browserType = BrowserType.CHROME;

    public static URL getSeleniumUrl() {
        URL url;
        try {
            url = new URL(String.format(HUB_URL_PATTERN, host, port));
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
        return url;
    }

    private static class BrowserTypeConverter implements IStringConverter<BrowserType> {
        @Override
        public BrowserType convert(String value) {
            return BrowserType.getByName(value);
        }
    }
}
