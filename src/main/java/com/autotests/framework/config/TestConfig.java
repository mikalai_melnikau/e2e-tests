package com.autotests.framework.config;

import com.beust.jcommander.Parameter;
import lombok.Getter;

import java.util.List;

public class TestConfig {

    @Parameter(names = {"--help", "-?"}, help = true, hidden = true)
    @Getter
    private static boolean help;

    @Parameter(names = {"-suites"}, variableArity = true,
            description = "A list of paths to one more XML files defining the tests")
    @Getter
    private static List<String> suites;
}
