package com.autotests.framework.exception;

public class CommonTestRuntimeException extends RuntimeException {

    public CommonTestRuntimeException(String message) {
        super(message);
    }

    public CommonTestRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }
}
