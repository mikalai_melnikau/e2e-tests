package com.autotests.framework.ui;

import com.autotests.framework.config.SeleniumConfig;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public final class Browser {

    public static final int ELEMENT_WAIT_TIMEOUT_SECONDS = 10;

    private static ThreadLocal<Browser> instance = new ThreadLocal<>();
    private WebDriver driver;

    private Browser() {
        start();
        enableLogging();
    }

    public static synchronized Browser getBrowser() {
        if (instance.get() == null) {
            instance.set(new Browser());
        }
        return instance.get();
    }

    public WebDriver getWrappedDriver() {
        return driver;
    }

    private void start() {
        RemoteWebDriver driver = new RemoteWebDriver(
                SeleniumConfig.getSeleniumUrl(), SeleniumConfig.getBrowserType().getCapabilities());
        driver.setFileDetector(new LocalFileDetector());
        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        this.driver = driver;
    }

    private void enableLogging() {
        EventFiringWebDriver driver = new EventFiringWebDriver(this.driver);
        driver.register(new BrowserEventListener());
        this.driver = driver;
    }

    public void stop() {
        try {
            if (driver != null) {
                driver.quit();
            }
        } finally {
            instance.remove();
        }
    }

    public void open(String url) {
        driver.get(url);
    }

    public void close() {
        driver.close();
    }

    public String getCurrentUrl() {
        return driver.getCurrentUrl();
    }

    public String getTitle() {
        return driver.getTitle();
    }

    public WebElement findElement(By by) {
        return driver.findElement(by);
    }

    public boolean isElementPresent(By by) {
        return driver.findElements(by).size() > 0;
    }

    public boolean isElementVisible(By by) {
        if (!isElementPresent(by)) {
            return false;
        }
        return findElement(by).isDisplayed();
    }

    public void waitForElementIsPresent(By by) {
        WebDriverWait wait = new WebDriverWait(driver, ELEMENT_WAIT_TIMEOUT_SECONDS);
        wait.until(ExpectedConditions.presenceOfElementLocated(by));
    }

    public void waitForElementIsVisible(By by) {
        WebDriverWait wait = new WebDriverWait(driver, ELEMENT_WAIT_TIMEOUT_SECONDS);
        wait.until(ExpectedConditions.visibilityOfElementLocated(by));
    }

    public void waitForElementIsNotVisible(By by) {
        WebDriverWait wait = new WebDriverWait(driver, ELEMENT_WAIT_TIMEOUT_SECONDS);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(by));
    }
}
