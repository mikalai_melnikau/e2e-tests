package com.autotests.framework.ui.element;

import com.autotests.framework.logger.Logger;
import com.autotests.framework.ui.Browser;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class Element {

    private By by;

    public Element(By by) {
        this.by = by;
    }

    public By getBy() {
        return by;
    }

    public WebElement getWrappedWebElement() {
        return Browser.getBrowser().findElement(by);
    }

    public boolean isPresent() {
        return Browser.getBrowser().isElementPresent(by);
    }

    public boolean isVisible() {
        return Browser.getBrowser().isElementVisible(by);
    }

    public void waitForAppear() {
        Browser.getBrowser().waitForElementIsVisible(by);
    }

    public String getText() {
        return getWrappedWebElement().getText();
    }

    public void click() {
        Logger.action(String.format("Click: '%s'", by));
        getWrappedWebElement().click();
    }

    public void typeValue(String value) {
        Logger.action(String.format("Type '%1$s' to '%2$s'", value, by));
        WebElement element = getWrappedWebElement();
        element.clear();
        element.sendKeys(value);
    }

    public void selectValue(String value) {
        Logger.action(String.format("Select '%1$s' for '%2$s'", value, by));
        getWrappedWebElement().sendKeys(value);
    }
}
