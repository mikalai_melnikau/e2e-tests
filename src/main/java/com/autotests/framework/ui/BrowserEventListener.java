package com.autotests.framework.ui;

import com.autotests.framework.logger.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.AbstractWebDriverEventListener;

public class BrowserEventListener extends AbstractWebDriverEventListener {

    @Override
    public void beforeNavigateTo(String url, WebDriver driver) {
        Logger.debug(String.format("Open: '%s'", url));
    }

    @Override
    public void beforeFindBy(By by, WebElement element, WebDriver driver) {
        Logger.debug(String.format("Find element: '%s'", by));
    }

    @Override
    public void onException(Throwable throwable, WebDriver driver) {
        Logger.trace(String.format("Exception: %s", throwable.getMessage()));
    }
}
