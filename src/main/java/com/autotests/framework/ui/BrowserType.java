package com.autotests.framework.ui;

import org.openqa.selenium.remote.DesiredCapabilities;

public enum BrowserType {
    CHROME("chrome") {
        @Override
        public DesiredCapabilities getCapabilities() {
            DesiredCapabilities capabilities = DesiredCapabilities.chrome();
            return capabilities;
        }
    },
    FIREFOX("firefox") {
        @Override
        public DesiredCapabilities getCapabilities() {
            DesiredCapabilities capabilities = DesiredCapabilities.firefox();
            return capabilities;
        }
    };

    private final String browserAlias;

    BrowserType(String browserAlias) {
        this.browserAlias = browserAlias;
    }

    public static BrowserType getByName(String value) {
        for (BrowserType type : BrowserType.values()) {
            if (type.browserAlias.equals(value)) {
                return type;
            }
        }
        throw new IllegalArgumentException(String.format("Incorrect alias '%s' of browser type", value));
    }

    public String getBrowserAlias() {
        return browserAlias;
    }

    public abstract DesiredCapabilities getCapabilities();
}
