package com.autotests.product.sslv.search;

import lombok.Getter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.HashMap;
import java.util.Map;

public class Filter {

    @Getter
    private final Map<Field, String> parameters = new HashMap<>();

    public Filter by(Field field, String value) {
        parameters.put(field, value);
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
