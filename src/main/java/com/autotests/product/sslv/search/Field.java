package com.autotests.product.sslv.search;

public enum Field {
    DESCRIPTION,
    MIN_PRICE,
    MAX_PRICE,
    AD_TYPE,
    LOCATION,
    PERIOD,
    SORTING_FIELD
}
