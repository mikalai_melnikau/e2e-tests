package com.autotests.product.sslv.search;

import com.autotests.framework.logger.Logger;
import com.autotests.product.sslv.search.screen.SearchScreen;

import java.util.Map;

public class Search {

    public static void by(Filter filter) {
        Logger.operation("Search from search screen: " + filter);
        SearchScreen searchScreen = new SearchScreen();
        for (Map.Entry<Field, String> entry : filter.getParameters().entrySet()) {
            switch (entry.getKey()) {
                case DESCRIPTION:
                    searchScreen.setDescription(entry.getValue());
                    break;
                case MIN_PRICE:
                    searchScreen.setMinPrice(entry.getValue());
                    break;
                case MAX_PRICE:
                    searchScreen.setMaxPrice(entry.getValue());
                    break;
                case AD_TYPE:
                    searchScreen.setAdType(entry.getValue());
                    break;
                case LOCATION:
                    searchScreen.setLocation(entry.getValue());
                    break;
                case PERIOD:
                    searchScreen.setPeriod(entry.getValue());
                    break;
                case SORTING_FIELD:
                    searchScreen.setSortingField(entry.getValue());
                    break;
            }
        }
        searchScreen.submit();
    }
}
