package com.autotests.product.sslv.search.screen;

import com.autotests.framework.ui.element.Element;
import com.autotests.product.sslv.ad.screen.AdvertisementListScreen;
import org.openqa.selenium.By;

public class SearchScreen {

    private static final String FIELD_LABEL_ELEMENT_LOCATOR_PATTERN = "%s/../preceding-sibling::td";
    private static final String DESCRIPTION_INPUT_LOCATOR = "//input[@name='txt']";
    private static final String SEARCH_SUGGESTION_ELEMENT_LOCATOR = "#preload_txt #cmp_1";
    private static final String MIN_PRICE_INPUT_LOCATOR = "input[name='topt[8][min]']";
    private static final String MAX_PRICE_INPUT_LOCATOR = "input[name='topt[8][max]']";
    private static final String AD_TYPE_SELECT_LOCATOR = "select[name='sid']";
    private static final String LOCATION_SELECT_LOCATOR = "select[name='search_region']";
    private static final String PERIOD_SELECT_LOCATOR = "select[name='pr']";
    private static final String SORTING_FIELD_SELECT_LOCATOR = "select[name='sort']";
    private static final String SEARCH_BUTTON_LOCATOR = "input[type='submit']";

    private Element descriptionLabelElement = new Element(By.xpath(
            String.format(FIELD_LABEL_ELEMENT_LOCATOR_PATTERN, DESCRIPTION_INPUT_LOCATOR)));
    private Element descriptionInput = new Element(By.xpath(DESCRIPTION_INPUT_LOCATOR));
    private Element searchSuggestionElement = new Element(By.cssSelector(SEARCH_SUGGESTION_ELEMENT_LOCATOR));
    private Element minPriceInput = new Element(By.cssSelector(MIN_PRICE_INPUT_LOCATOR));
    private Element maxPriceInput = new Element(By.cssSelector(MAX_PRICE_INPUT_LOCATOR));
    private Element adTypeSelect = new Element(By.cssSelector(AD_TYPE_SELECT_LOCATOR));
    private Element locationSelect = new Element(By.cssSelector(LOCATION_SELECT_LOCATOR));
    private Element periodSelect = new Element(By.cssSelector(PERIOD_SELECT_LOCATOR));
    private Element sortingFieldSelect = new Element(By.cssSelector(SORTING_FIELD_SELECT_LOCATOR));
    private Element searchButton = new Element(By.cssSelector(SEARCH_BUTTON_LOCATOR));

    public SearchScreen setDescription(String value) {
        descriptionInput.typeValue(value);
        searchSuggestionElement.waitForAppear();
        descriptionLabelElement.click();
        return this;
    }

    public SearchScreen setMinPrice(String value) {
        minPriceInput.typeValue(value);
        return this;
    }

    public SearchScreen setMaxPrice(String value) {
        maxPriceInput.typeValue(value);
        return this;
    }

    public SearchScreen setAdType(String value) {
        adTypeSelect.selectValue(value);
        return this;
    }

    public SearchScreen setLocation(String value) {
        locationSelect.selectValue(value);
        return this;
    }

    public SearchScreen setPeriod(String value) {
        periodSelect.selectValue(value);
        return this;
    }

    public SearchScreen setSortingField(String value) {
        sortingFieldSelect.selectValue(value);
        return this;
    }

    public AdvertisementListScreen submit() {
        searchButton.click();
        return new AdvertisementListScreen();
    }
}
