package com.autotests.product.sslv.common;

import com.autotests.framework.logger.Logger;
import com.autotests.product.sslv.ad.AdvertisementCategory;
import com.autotests.product.sslv.common.context.ActorContext;
import com.autotests.product.sslv.common.screen.HomeScreen;

public class Actor {

    public static void asAnonymousUser() {
        Logger.operation("Actor as anonymous user");
        ActorContext.clearContext();
        new HomeScreen().open();
    }

    public static void asAnonymousUser(Language language) {
        asAnonymousUser();
        switchLanguage(language);
    }

    public static void navigateToSearch() {
        Logger.operation("Actor navigates to 'Search'");
        new HomeScreen().openSearch();
    }

    public static void navigateToBookmarks() {
        Logger.operation("Actor navigates to 'Bookmarks'");
        new HomeScreen().openBookmarks();
    }

    public static void switchLanguage(Language language) {
        Logger.operation(String.format("Actor use '%s' language", language.name()));
        new HomeScreen().setLanguage(language);
        ActorContext.getContext().setCurrentLanguage(language);
    }

    public static void navigateToCategory(AdvertisementCategory adCategory) {
        Logger.operation(String.format("Actor navigates to '%s' advertisement category", adCategory.getPath()));
        new HomeScreen().openAdCategory(adCategory.getPath());
    }
}
