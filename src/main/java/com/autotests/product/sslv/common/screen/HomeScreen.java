package com.autotests.product.sslv.common.screen;

import com.autotests.framework.ui.Browser;
import com.autotests.framework.ui.element.Element;
import com.autotests.product.sslv.ad.screen.AdvertisementListScreen;
import com.autotests.product.sslv.common.Language;
import com.autotests.product.sslv.common.context.ActorContext;
import com.autotests.product.sslv.common.context.ProductConfig;
import com.autotests.product.sslv.search.screen.SearchScreen;
import org.openqa.selenium.By;

public class HomeScreen {

    private static final String MAIN_MENU_LINK_ELEMENT_LOCATOR_PATTERN =
            "//*[@class='menu_main']/*[contains(@href, '/%s/')]";
    private static final String LANGUAGE_LINK_ELEMENT_LOCATOR = ".menu_lang > .a_menu";
    private static final String AD_CATEGORY_LINK_ELEMENT_LOCATOR_PATTERN = "//*[@href='/%1$s/%2$s/' and not(img)]";

    private Element searchLink = new Element(By.xpath(
            String.format(MAIN_MENU_LINK_ELEMENT_LOCATOR_PATTERN, "search")));
    private Element bookmarksLink = new Element(By.xpath(
            String.format(MAIN_MENU_LINK_ELEMENT_LOCATOR_PATTERN, "favorites")));
    private Element languageLink = new Element(By.cssSelector(LANGUAGE_LINK_ELEMENT_LOCATOR));

    public HomeScreen open() {
        Browser.getBrowser().open(ProductConfig.getHomeUrl());
        return this;
    }

    public SearchScreen openSearch() {
        searchLink.click();
        return new SearchScreen();
    }

    public AdvertisementListScreen openBookmarks() {
        bookmarksLink.click();
        return new AdvertisementListScreen();
    }

    public HomeScreen setLanguage(Language language) {
        if (languageLink.getText().equals(language.name())) {
            languageLink.click();
        }
        return this;
    }

    public HomeScreen openAdCategory(String adCategoryPath) {
        Element adCategoryLink = new Element(By.xpath(String.format(AD_CATEGORY_LINK_ELEMENT_LOCATOR_PATTERN,
                ActorContext.getContext().getCurrentLanguage().getCode(), adCategoryPath)));
        adCategoryLink.click();
        return this;
    }
}
