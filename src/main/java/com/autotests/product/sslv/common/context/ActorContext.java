package com.autotests.product.sslv.common.context;

import com.autotests.product.sslv.account.bo.Account;
import com.autotests.product.sslv.common.Language;
import lombok.Getter;
import lombok.Setter;

public class ActorContext {

    private static ThreadLocal<ActorContext> instance = new ThreadLocal<>();

    @Getter
    @Setter
    private Account currentActor;
    @Getter
    @Setter
    private Language currentLanguage;

    private ActorContext() {
    }

    public static synchronized ActorContext getContext() {
        if (instance.get() == null) {
            instance.set(new ActorContext());
        }
        return instance.get();
    }

    public static void clearContext() {
        instance.remove();
    }
}
