package com.autotests.product.sslv.common;

import lombok.Getter;

public enum Language {

    LV("lv"),
    RU("ru");

    @Getter
    private final String code;

    Language(String code) {
        this.code = code;
    }
}
