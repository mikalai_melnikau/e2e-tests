package com.autotests.product.sslv.ad;

import lombok.Getter;

public enum AdvertisementCategory {

    ELECTRONICS("electronics");

    @Getter
    private final String path;

    AdvertisementCategory(String path) {
        this.path = path;
    }
}
