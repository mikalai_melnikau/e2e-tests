package com.autotests.product.sslv.ad.screen;

import com.autotests.framework.ui.Browser;
import com.autotests.framework.ui.element.Element;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import javax.swing.text.html.HTML;
import java.util.ArrayList;
import java.util.List;

public class AdvertisementListScreen {

    private static final int AD_DESCRIPTION_MIN_DISPLAYED_LENGTH = 60;

    private static final String AD_ELEMENTS_LOCATOR = "//tr[contains(@id, 'tr_')]";
    private static final String AD_DESCRIPTION_ELEMENT_LOCATOR_PATTERN = "#%s .am";
    private static final String AD_IMAGE_LINK_ELEMENT_LOCATOR_PATTERN = "#%s img";
    private static final String AD_CHECKBOX_LOCATOR_PATTERN = "#%s input";
    private static final String ADD_TO_BOOKMARKS_BUTTON_LOCATOR = "#a_fav_sel";
    private static final String ACCEPT_NOTIFICATION_BUTTON_LOCATOR = "#alert_ok";

    private Element addToBookmarksButton = new Element(By.cssSelector(ADD_TO_BOOKMARKS_BUTTON_LOCATOR));
    private Element acceptNotificationButton = new Element(By.cssSelector(ACCEPT_NOTIFICATION_BUTTON_LOCATOR));

    public List<String> getAdIds() {
        List<WebElement> elements = Browser.getBrowser().getWrappedDriver().findElements(
                By.xpath(AD_ELEMENTS_LOCATOR));
        List<String> adIds = new ArrayList<>();
        for (WebElement element : elements) {
            adIds.add(element.getAttribute(HTML.Attribute.ID.toString()));
        }
        return adIds;
    }

    public List<String> getRandomAdIds(int count) {
        List<WebElement> elements = Browser.getBrowser().getWrappedDriver().findElements(
                By.xpath(AD_ELEMENTS_LOCATOR));
        List<String> adIds = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            int id = RandomUtils.nextInt(NumberUtils.INTEGER_ZERO, elements.size());
            adIds.add(elements.get(id).getAttribute(HTML.Attribute.ID.toString()));
            elements.remove(id);
        }
        return adIds;
    }

    public String getAdDescription(String adId) {
        Element adDescriptionElement = new Element(By.cssSelector(
                String.format(AD_DESCRIPTION_ELEMENT_LOCATOR_PATTERN, adId)));
        String adDescription = adDescriptionElement.getText();
        if (adDescription.length() > AD_DESCRIPTION_MIN_DISPLAYED_LENGTH) {
            adDescription = adDescription.substring(NumberUtils.INTEGER_ZERO, AD_DESCRIPTION_MIN_DISPLAYED_LENGTH);
        }
        return adDescription;
    }

    public String getAdImageUrl(String adId) {
        Element adImageLink = new Element(By.cssSelector(String.format(AD_IMAGE_LINK_ELEMENT_LOCATOR_PATTERN, adId)));
        return adImageLink.getWrappedWebElement().getAttribute(HTML.Attribute.SRC.toString());
    }

    public AdvertisementListScreen setAdCheckbox(String adId, boolean value) {
        Element adCheckbox = new Element(By.cssSelector(String.format(AD_CHECKBOX_LOCATOR_PATTERN, adId)));
        adCheckbox.click();
        return this;
    }

    public AdvertisementListScreen addToBookmarks() {
        addToBookmarksButton.click();
        acceptNotificationButton.waitForAppear();
        acceptNotificationButton.click();
        return this;
    }
}
