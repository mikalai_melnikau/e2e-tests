package com.autotests.product.sslv.ad.service;

import com.autotests.framework.logger.Logger;
import com.autotests.product.sslv.ad.bo.Advertisement;
import com.autotests.product.sslv.ad.bo.AdvertisementBuilder;
import com.autotests.product.sslv.ad.screen.AdvertisementListScreen;

import java.util.ArrayList;
import java.util.List;

public class AdvertisementUiService {

    public List<Advertisement> readAds() {
        Logger.operation("Read all advertisements from page");
        AdvertisementListScreen adListScreen = new AdvertisementListScreen();
        return readAds(adListScreen.getAdIds());
    }

    public List<Advertisement> readRandomAds(int count) {
        Logger.operation(String.format("Read %d random advertisement(s) from page", count));
        AdvertisementListScreen adListScreen = new AdvertisementListScreen();
        return readAds(adListScreen.getRandomAdIds(count));
    }

    public List<Advertisement> readAds(List<String> adIds) {
        List<Advertisement> ads = new ArrayList<>();
        for (String adId : adIds) {
            ads.add(readAd(adId));
        }
        return ads;
    }

    public Advertisement readAd(String adId) {
        AdvertisementListScreen adListScreen = new AdvertisementListScreen();
        Advertisement ad = new AdvertisementBuilder()
                .id(adId)
                .description(adListScreen.getAdDescription(adId))
                .imageUrl(adListScreen.getAdImageUrl(adId))
                .build();
        Logger.info("Advertisement red: " + ad.toString());
        return ad;
    }

    public void addToBookmarks(List<Advertisement> ads) {
        Logger.operation("Add advertisement(s) to bookmarks");
        Logger.info("Advertisement(s): " + ads.toString());
        AdvertisementListScreen adListScreen = new AdvertisementListScreen();
        for (Advertisement ad : ads) {
            adListScreen.setAdCheckbox(ad.getId(), true);
        }
        adListScreen.addToBookmarks();
    }
}
