package com.autotests.product.sslv.ad.bo;

public class AdvertisementBuilder {

    private Advertisement advertisement = new Advertisement();

    public AdvertisementBuilder id(String id) {
        advertisement.setId(id);
        return this;
    }

    public AdvertisementBuilder description(String description) {
        advertisement.setDescription(description);
        return this;
    }

    public AdvertisementBuilder imageUrl(String imageUrl) {
        advertisement.setImageUrl(imageUrl);
        return this;
    }

    public Advertisement build() {
        return advertisement;
    }
}
